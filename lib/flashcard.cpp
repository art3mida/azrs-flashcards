#include "flashcard.h"
#include <iostream>

Flashcard::Flashcard() {}

void Flashcard::set_question(const std::string& question) {
    this->m_question = question;
}

std::string Flashcard::get_question() const {
    return this->m_question;
}

void Flashcard::set_option(const char option_code, const std::string& option_content) {
    this->m_options.insert(std::make_pair(option_code, option_content));
}

std::map<char, std::string> Flashcard::get_options() {
    return this->m_options;
}

void Flashcard::set_correct_answer(const char correct_answer) {
    this->m_correct_answer = correct_answer;
}

char Flashcard::get_correct_answer() {
    return this->m_correct_answer;
}

bool Flashcard::is_correct(const char selected_option) {
    return this->m_correct_answer == selected_option;
}

bool Flashcard::ask() {
    std::cout << "NEXT QUESTION:" << std::endl;
    std::cout << this->m_question << std::endl;
    for (const auto& elem : this->m_options) {
        std::cout << elem.first << ") " << elem.second << std::endl;
    }
    std::cout << "Your answer is: ";

    char user_selected;
    std::cin >> user_selected;
    if (this->is_correct(user_selected)) {
        std::cout << "CORRECT!" << std::endl;
        return true;
    }
    else {
        std::cout << "WRONG! THe correct answer was " 
                    << this->m_correct_answer 
                    << ")." << std::endl;
        return false;
    }
}

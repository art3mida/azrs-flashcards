cmake_minimum_required(VERSION 3.16)
project(Flashcards VERSION 1.0.0)

add_library(
    flashcard_lib STATIC
    flashcard.h
    flashcard.cpp
)

target_include_directories(flashcard_lib PUBLIC "${CMAKE_CURRENT_SOURCE_DIRECTORY}")

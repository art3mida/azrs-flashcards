#include <map>

class Flashcard {
 public:
    Flashcard();

    void set_question(const std::string& question);
    std::string get_question() const;
    void set_option(const char option_code, const std::string& option_content);
    std::map<char, std::string> get_options();
    void set_correct_answer(const char correct_answer);
    char get_correct_answer();

    bool is_correct(const char selected_option);
    bool ask();

 private:
    std::string m_question;
    std::map<char, std::string> m_options;
    char m_correct_answer;
};
#include "flashcard.h"

int main() {
    Flashcard* card = new Flashcard();

    card->set_question("Which of these is the correct way to write the title of a commit message?");
    card->set_option('a', "implemented to_string");
    card->set_option('b', "fix: Implemented to_string method of Flashcard class");
    card->set_option('c', "Implement to_string method of Flashcard class to return the question with appended answer options, where the option code of the correct answer is circled.");
    card->set_option('d', "feat: Implement to_string method of Flashcard class");
    card->set_correct_answer('d');

    card->ask();

    return 0;
}